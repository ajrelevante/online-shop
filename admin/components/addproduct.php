<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/download.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Add Product
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->

  <!-- CSS Files -->
  <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="../assets/css/now-ui-dashboard.css?v=1.1.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../assets/demo/demo.css" rel="stylesheet" />
</head>

<div class="col-md-4 offset-md-4">
  <form>
    <div class="form-group" >
      <label for="addproduct">Add Product</label>
      <input type="text" class="form-control" id="addproduct"  placeholder="Enter Product">
    </div>
    <div class="form-group">
      <label for="addprice">Price</label>
      <input type="text" class="form-control" id="addprice" placeholder="Enter Price">
    </div>
    <div class="form-group">
      <label for="adddescription">Description</label>
      <input type="text" class="form-control" id="adddescription" placeholder="Enter Description">
    </div>
    <button type="submit" class="btn btn-primary">Add</button>
  </form>
</div>
