
<!DOCTYPE html>
<html lang="en">

<?php
include('components/style.php');
 ?>

<body>
	<!-- HEADER -->
	<?php
	include('components/header.php');
	 ?>
	<!-- /HEADER -->

	<!-- NAVIGATION -->
	<?php
	include('components/side-menubar.php');
	 ?>
	<!-- /NAVIGATION -->

	<!-- HOME -->

	<!-- /HOME -->



	<!-- FOOTER -->
	<?php
	include('components/footer.php');
	 ?>
	<!-- /FOOTER -->

	<!-- jQuery Plugins -->
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/nouislider.min.js"></script>
	<script src="js/jquery.zoom.min.js"></script>
	<script src="js/main.js"></script>

</body>

</html>
